# Curso prático de introducao a Docker Containers - Adriano da Silva Fonseca

## Porque eu deveria aprender Docker?

Hoje 4.5 bilhões de containers são rodados. Na indústria mundial algo entre 40% a 70% dos serviços usam containers em produção. Uma das mais rápidas adoções dá história da tecnologia, tendo em vista que Docker foi lançado em março de 2013. Esse é um curso INTRODUTÓRIO de Docker nesse curso estaremos explorando a Docker CLI e a utilizaremos para interagir com os objetos container e images. Aprenderemos tambem como salvar o estado de um container através do uso de volumes. Ao fim do curso apos uma revisao detalhada você tera a chance de validar seus conhecimentos com exercício que tambem sera resolvido durante o curso.


## O que é possível fazer com Docker?

- Restringir o uso de processamento e memória por parte das nossas aplicações;

- Escalar sua aplicação com um comando, e ser capaz de responder rapidamente a demandas sazonais;

- Ter sua aplicação constantemente monitorada e com capacidade de auto-recuperação e caso de uma falha de hardware ou software;

- Ter a capacidade de testar seu ambiente real, o que vai estar de fato em produção, acabando com o famoso "mas funcionou na minha máquina";

- Ter flexibilidade para testar novas tecnologias, sem depender de processos demorados de instalação de servidores;

### O que você aprenderá
Entender, criar e gerenciar Containers
Entender, criar e gerenciar imagens
Ender e gerenciar volumes
Há algum requisito ou pré-requisito para o curso?
Sistema operacional Linux
Noções de virtualização
Noções de redes de computador
Para quem é este curso:
Desenvolvedores de Software
Administradores de Sistemas
Profissionais de DevOps

### **https://www.udemy.com/course/docker-para-devops-atualizado-docker-ce-1706/learn/lecture/7148934#overview**
